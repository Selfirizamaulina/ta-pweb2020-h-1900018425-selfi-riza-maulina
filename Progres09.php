<!DOCTYPE html>
<html>
<head>
	<title>Selfi Riza Maulina</title>
		<style type="text/css">
			body{
				margin:0;
				padding: 0;
				background-image: url(pewpew.jpg);
				background-size: cover;
				background-position: 0 -100px;
				color: orange;
				font-size: 25px;
				font-weight: bold;
				text-align: center;
			}
			h1{
				text-align: center;
				background-color: grey(0,0,0,0.7);
				color: rgba(0,0,0,0.7);
				text-shadow: 0 2px 2px white;
				font-weight: bold;
				text-transform: uppercase;
				font-size: 50px;
				margin-top: -5px;
				box-shadow: 0 1px 3px rgba(0,0,0,0.5);
			}
			form{
				background-color: gray(0,0,0,0.5);
				width: 500px;
				height: 100px;
				margin: 40px auto;
				padding: 20px;
				box-sizing: border-box;
				text-align: center;
				font-weight: bold;
				color: orange;
				font-size: 20px;
			}
			form #input{
				margin-bottom: 15px;
			}
			.hasil{
				background-color: gray(0,0,0,0.7);
				width: 500px;
				margin: auto;
				padding: 20px;
				box-sizing: border-box;
			}
		</style>
</head>
<body>
	<h1>Dasar-Dasar PHP</h1>
	<form action="#" method="post">
		<label for="input">Masukan Bilangan : </label><input type="text" name="input" id="input"><br>
		<button type="submit" name="submit" id="submit">Submit Bilangan</button>
	</form>

	<div class="hasil">
	<?php 

	$n=isset($_GET['input'])?$_GET['input']:1;

	for($angka=1;$angka<=$n;$angka++){
		$prima=true;
		for($i=2;$i<$angka;$i++){
			if($angka%$i==0){
				$prima=false;
			}
		}
	}
	if($prima==true){
		echo "<br> bilangan yang anda inputkan merupakan bilangan prima";
	}else{
		echo "<br> bilangan yang anda inputkan bukan bilangan prima";
	}

	 ?>
	</div>
</body>
</html>

